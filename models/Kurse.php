<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kurse".
 *
 * @property int $IdKurse
 * @property string $Name
 * @property int $MaxTeilnehmer
 * @property string $Schuljahr
 * @property string $Semester
 * @property string $Rhytmus
 * @property string $Wochentag
 * @property int $SchulstundeBeginn
 * @property int $DauerWochenstunden
 * @property string $Hauptkategorie
 * @property string $Subkategorie
 * @property string $SchulstufeHaupt
 * @property string $SchulstufeSub
 * @property string $SchulformHaupt
 * @property string $SchulformSub
 * @property string $Bemerkungen
 * @property int $Lehrer_IdLehrer
 *
 * @property Kursanmeldung[] $kursanmeldungs
 * @property Lehrer $lehrerIdLehrer
 */
class Kurse extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kurse';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Name', 'MaxTeilnehmer', 'Schuljahr', 'Semester', 'Rhytmus', 'Wochentag', 'SchulstundeBeginn', 'DauerWochenstunden', 'Hauptkategorie', 'SchulstufeHaupt', 'SchulformHaupt', 'Lehrer_IdLehrer'], 'required'],
            [['MaxTeilnehmer', 'SchulstundeBeginn', 'DauerWochenstunden', 'Lehrer_IdLehrer'], 'integer'],
            [['Schuljahr', 'Semester', 'Rhytmus', 'Wochentag', 'Hauptkategorie', 'Subkategorie', 'SchulstufeHaupt', 'SchulstufeSub', 'SchulformHaupt', 'SchulformSub', 'Bemerkungen'], 'string'],
            [['Name'], 'string', 'max' => 150],
            [['Lehrer_IdLehrer'], 'exist', 'skipOnError' => true, 'targetClass' => Lehrer::className(), 'targetAttribute' => ['Lehrer_IdLehrer' => 'IdLehrer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdKurse' => 'Id Kurse',
            'Name' => 'Name',
            'MaxTeilnehmer' => 'Max Teilnehmer',
            'Schuljahr' => 'Schuljahr',
            'Semester' => 'Semester',
            'Rhytmus' => 'Rhytmus',
            'Wochentag' => 'Wochentag',
            'SchulstundeBeginn' => 'Schulstunde Beginn',
            'DauerWochenstunden' => 'Dauer Wochenstunden',
            'Hauptkategorie' => 'Hauptkategorie',
            'Subkategorie' => 'Subkategorie',
            'SchulstufeHaupt' => 'Schulstufe Haupt',
            'SchulstufeSub' => 'Schulstufe Sub',
            'SchulformHaupt' => 'Schulform Haupt',
            'SchulformSub' => 'Schulform Sub',
            'Bemerkungen' => 'Bemerkungen',
            'Lehrer_IdLehrer' => 'Lehrer  Id Lehrer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKursanmeldungs()
    {
        return $this->hasMany(Kursanmeldung::className(), ['Kurse_Id' => 'IdKurse']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLehrerIdLehrer()
    {
        return $this->hasOne(Lehrer::className(), ['IdLehrer' => 'Lehrer_IdLehrer']);
    }

    /**
     * @return string
     */
    public function getLehrerName() {
        $lehrer = Lehrer::findOne($this->Lehrer_IdLehrer);
        $lehrerName = $lehrer->Nachname . ' ' . $lehrer->Vorname;
        return $lehrerName;
    }

    /**
     * @return string
     */
    public function getLehrerKuerzel() {
        return Lehrer::findOne($this->Lehrer_IdLehrer)->Lehrerkuerzel;
    }
}
