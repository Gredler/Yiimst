<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kursanmeldung".
 *
 * @property int $IdKursanmeldung
 * @property int $Schueler_Id
 * @property int $Kurse_Id
 * @property string $Anmeldedatum
 * @property string $Abmeldedatum
 *
 * @property Kurse $kurse
 * @property Schueler $schueler
 * @property Kursbesuch[] $kursbesuches
 */
class Kursanmeldung extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kursanmeldung';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Schueler_Id', 'Kurse_Id', 'Anmeldedatum'], 'required'],
            [['Schueler_Id', 'Kurse_Id'], 'integer'],
            [['Anmeldedatum', 'Abmeldedatum'], 'safe'],
            [['Kurse_Id'], 'exist', 'skipOnError' => true, 'targetClass' => Kurse::className(), 'targetAttribute' => ['Kurse_Id' => 'IdKurse']],
            [['Schueler_Id'], 'exist', 'skipOnError' => true, 'targetClass' => Schueler::className(), 'targetAttribute' => ['Schueler_Id' => 'IdSchueler']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdKursanmeldung' => 'Id Kursanmeldung',
            'Schueler_Id' => 'Schueler  ID',
            'Kurse_Id' => 'Kurse  ID',
            'Anmeldedatum' => 'Anmeldedatum',
            'Abmeldedatum' => 'Abmeldedatum',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKurse()
    {
        return $this->hasOne(Kurse::className(), ['IdKurse' => 'Kurse_Id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchueler()
    {
        return $this->hasOne(Schueler::className(), ['IdSchueler' => 'Schueler_Id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKursbesuches()
    {
        return $this->hasMany(Kursbesuch::className(), ['Anmeldung_Id' => 'IdKursanmeldung']);
    }

    /**
     * @return string
     */
    public function getSchuelerName() {
        $schueler = Schueler::findOne($this->Schueler_Id);

        $schuelerName = $schueler->Nachname . ' ' . $schueler->Vorname;
        return $schuelerName;
    }

    /**
     * @return string
     */
    public function getSchuelerSKZ() {
        return Schueler::findOne($this->Schueler_Id)->SKZ;
    }

    /**
     * @return string
     */
    public function getKursName() {
        return Kurse::findOne($this->Kurse_Id)->Name;
    }


    /**
     * @return string
     */
    public function getKursAndSchueler() {
        $kurs = Kurse::findOne($this->Kurse_Id)->Name;
        $schueler = Schueler::findOne($this->Schueler_Id);
        $schuelerName = $schueler->Nachname . ' ' . $schueler->Vorname . ', ' . $schueler->SKZ;

        return $kurs . ' | ' . $schuelerName;
    }
}
