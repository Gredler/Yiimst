<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lehrer".
 *
 * @property int $IdLehrer
 * @property string $Lehrerkuerzel
 * @property string $Vorname
 * @property string $Nachname
 * @property int $ImstKontingent
 * @property int $User_Id
 *
 * @property Kursbesuch[] $kursbesuches
 * @property Kurse[] $kurses
 * @property Benutzer $user
 */
class Lehrer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lehrer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Lehrerkuerzel', 'Vorname', 'Nachname', 'User_Id'], 'required'],
            [['ImstKontingent', 'User_Id'], 'integer'],
            [['Lehrerkuerzel'], 'string', 'max' => 5],
            [['Vorname', 'Nachname'], 'string', 'max' => 45],
            [['User_Id'], 'exist', 'skipOnError' => true, 'targetClass' => Benutzer::className(), 'targetAttribute' => ['User_Id' => 'IdBenutzer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdLehrer' => 'Id Lehrer',
            'Lehrerkuerzel' => 'Lehrerkuerzel',
            'Vorname' => 'Vorname',
            'Nachname' => 'Nachname',
            'ImstKontingent' => 'Imst Kontingent',
            'User_Id' => 'User  ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKursbesuches()
    {
        return $this->hasMany(Kursbesuch::className(), ['Lehrer_Id' => 'IdLehrer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKurses()
    {
        return $this->hasMany(Kurse::className(), ['Lehrer_IdLehrer' => 'IdLehrer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Benutzer::className(), ['IdBenutzer' => 'User_Id']);
    }

	/**
	 * @return string
	 */
    public function getUserName() {
    	return Benutzer::findOne($this->User_Id)->username;
    }
}
