<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kursbesuch;

/**
 * KursbesuchSearch represents the model behind the search form of `app\models\Kursbesuch`.
 */
class KursbesuchSearch extends Kursbesuch
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdKursbesuch', 'Schulstunden', 'Anmeldung_Id', 'Lehrer_Id'], 'integer'],
            [['Datum'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kursbesuch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'IdKursbesuch' => $this->IdKursbesuch,
            'Datum' => $this->Datum,
            'Schulstunden' => $this->Schulstunden,
            'Anmeldung_Id' => $this->Anmeldung_Id,
            'Lehrer_Id' => $this->Lehrer_Id,
        ]);

        return $dataProvider;
    }
}
