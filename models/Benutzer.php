<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "benutzer".
 *
 * @property int        $IdBenutzer
 * @property string     $username
 * @property string     $password
 * @property string     $type
 * @property string     $authKey
 *
 * @property Lehrer[]   $lehrers
 * @property Schueler[] $schuelers
 */
class Benutzer extends \yii\db\ActiveRecord implements IdentityInterface
{

	public $hashPassword = false;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'benutzer';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['username', 'password', 'type'], 'required'],
			[['username'], 'string', 'max' => 150],
			[['password'], 'string', 'max' => 200],
			[['type'], 'string'],
			[['authKey'], 'string', 'max' => 255],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'IdBenutzer' => 'Id Benutzer',
			'username'   => 'Username',
			'password'   => 'Password',
			'type'       => 'Type',
			'authKey'    => 'Auth Key',
		];
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert))
		{
			if ($this->hashPassword)
			{
				$this->password = Yii::$app->security->generatePasswordHash($this->password, 10);
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	public function afterSave($insert, $changedAttributes)
	{
		if ($insert)
		{ //true, wenn neuer Datensatz angelegt wurde (nicht update)
			$auth = Yii::$app->authManager;
			if ($this->type == "admin")
			{
				$role = $auth->getRole('admin');
				$auth->assign($role, $this->IdBenutzer);
			}
			elseif ($this->type == "lehrer")
			{
				$role = $auth->getRole('lehrer');
				$auth->assign($role, $this->IdBenutzer);
			}
			else
			{
				$role = $auth->getRole('schueler');
				$auth->assign($role, $this->IdBenutzer);
			}
			parent::afterSave($insert, $changedAttributes);
		}
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLehrers()
	{
		return $this->hasMany(Lehrer::className(), ['User_Id' => 'IdBenutzer']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSchuelers()
	{
		return $this->hasMany(Schueler::className(), ['User_Id' => 'IdBenutzer']);
	}

	/**
	 * Finds an identity by the given ID.
	 *
	 * @param string|int $id the ID to be looked for
	 *
	 * @return IdentityInterface the identity object that matches the given ID.
	 * Null should be returned if such an identity cannot be found
	 * or the identity is not in an active state (disabled, deleted, etc.)
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Finds an identity by the given token.
	 *
	 * @param mixed $token the token to be looked for
	 * @param mixed $type  the type of the token. The value of this parameter depends on the implementation.
	 *                     For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
	 *
	 * @return IdentityInterface the identity object that matches the given token.
	 * Null should be returned if such an identity cannot be found
	 * or the identity is not in an active state (disabled, deleted, etc.)
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * Returns an ID that can uniquely identify a user identity.
	 * @return string|int an ID that uniquely identifies a user identity.
	 */
	public function getId()
	{
		return $this->IdBenutzer;
	}

	/**
	 * Returns a key that can be used to check the validity of a given identity ID.
	 *
	 * The key should be unique for each individual user, and should be persistent
	 * so that it can be used to check the validity of the user identity.
	 *
	 * The space of such keys should be big enough to defeat potential identity attacks.
	 *
	 * This is required if [[User::enableAutoLogin]] is enabled.
	 * @return string a key that is used to check the validity of a given identity ID.
	 * @see validateAuthKey()
	 */
	public function getAuthKey()
	{
		return $this->authKey;
	}

	/**
	 * Validates the given auth key.
	 *
	 * This is required if [[User::enableAutoLogin]] is enabled.
	 *
	 * @param string $authKey the given auth key
	 *
	 * @return bool whether the given auth key is valid.
	 * @see getAuthKey()
	 */
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}

	public function validatePassword($password)
	{
		return \Yii::$app->security->validatePassword($password, $this->password);
	}

	/**
	 * Bereinigung RBAC: Rollenzuweisung beim Löschen des Benutzers wieder entziehen
	 * @return bool
	 */
	public function beforeDelete()
	{
		if (!parent::beforeDelete())
		{
			return false;
		}
		$auth = Yii::$app->authManager;
		$auth->revokeAll($this->IdBenutzer);

		return true;
	}
}
