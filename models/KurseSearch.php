<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kurse;

/**
 * KurseSearch represents the model behind the search form of `app\models\Kurse`.
 */
class KurseSearch extends Kurse
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdKurse', 'MaxTeilnehmer', 'SchulstundeBeginn', 'DauerWochenstunden', 'Lehrer_IdLehrer'], 'integer'],
            [['Name', 'Schuljahr', 'Semester', 'Rhytmus', 'Wochentag', 'Hauptkategorie', 'Subkategorie', 'SchulstufeHaupt', 'SchulstufeSub', 'SchulformHaupt', 'SchulformSub', 'Bemerkungen'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kurse::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'IdKurse' => $this->IdKurse,
            'MaxTeilnehmer' => $this->MaxTeilnehmer,
            'SchulstundeBeginn' => $this->SchulstundeBeginn,
            'DauerWochenstunden' => $this->DauerWochenstunden,
            'Lehrer_IdLehrer' => $this->Lehrer_IdLehrer,
        ]);

        $query->andFilterWhere(['like', 'Name', $this->Name])
            ->andFilterWhere(['like', 'Schuljahr', $this->Schuljahr])
            ->andFilterWhere(['like', 'Semester', $this->Semester])
            ->andFilterWhere(['like', 'Rhytmus', $this->Rhytmus])
            ->andFilterWhere(['like', 'Wochentag', $this->Wochentag])
            ->andFilterWhere(['like', 'Hauptkategorie', $this->Hauptkategorie])
            ->andFilterWhere(['like', 'Subkategorie', $this->Subkategorie])
            ->andFilterWhere(['like', 'SchulstufeHaupt', $this->SchulstufeHaupt])
            ->andFilterWhere(['like', 'SchulstufeSub', $this->SchulstufeSub])
            ->andFilterWhere(['like', 'SchulformHaupt', $this->SchulformHaupt])
            ->andFilterWhere(['like', 'SchulformSub', $this->SchulformSub])
            ->andFilterWhere(['like', 'Bemerkungen', $this->Bemerkungen]);

        return $dataProvider;
    }
}
