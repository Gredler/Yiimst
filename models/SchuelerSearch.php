<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Schueler;

/**
 * SchuelerSearch represents the model behind the search form of `app\models\Schueler`.
 */
class SchuelerSearch extends Schueler
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdSchueler', 'ImstKontingent', 'User_Id'], 'integer'],
            [['SKZ', 'Vorname', 'Nachname', 'Geburtsdatum'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Schueler::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'IdSchueler' => $this->IdSchueler,
            'Geburtsdatum' => $this->Geburtsdatum,
            'ImstKontingent' => $this->ImstKontingent,
            'User_Id' => $this->User_Id,
        ]);

        $query->andFilterWhere(['like', 'SKZ', $this->SKZ])
            ->andFilterWhere(['like', 'Vorname', $this->Vorname])
            ->andFilterWhere(['like', 'Nachname', $this->Nachname]);

        return $dataProvider;
    }
}
