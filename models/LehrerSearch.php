<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lehrer;

/**
 * LehrerSearch represents the model behind the search form of `app\models\Lehrer`.
 */
class LehrerSearch extends Lehrer
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdLehrer', 'ImstKontingent', 'User_Id'], 'integer'],
            [['Lehrerkuerzel', 'Vorname', 'Nachname'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lehrer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['user']);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'IdLehrer' => $this->IdLehrer,
            'ImstKontingent' => $this->ImstKontingent,
            'User_Id' => $this->User_Id,
        ]);

        $query->andFilterWhere(['like', 'Lehrerkuerzel', $this->Lehrerkuerzel])
            ->andFilterWhere(['like', 'Vorname', $this->Vorname])
            ->andFilterWhere(['like', 'Nachname', $this->Nachname]);

        return $dataProvider;
    }
}
