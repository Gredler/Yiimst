<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schueler".
 *
 * @property int $IdSchueler
 * @property string $SKZ
 * @property string $Vorname
 * @property string $Nachname
 * @property string $Geburtsdatum
 * @property int $ImstKontingent
 * @property int $User_Id
 *
 * @property Kursanmeldung[] $kursanmeldungs
 * @property Benutzer $user
 */
class Schueler extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'schueler';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SKZ', 'Vorname', 'Nachname', 'Geburtsdatum', 'User_Id'], 'required'],
            [['IdSchueler', 'ImstKontingent', 'User_Id'], 'integer'],
            [['Geburtsdatum'], 'safe'],
            [['SKZ'], 'string', 'max' => 10],
            [['Vorname', 'Nachname'], 'string', 'max' => 45],
            [['SKZ'], 'unique'],
            [['IdSchueler'], 'unique'],
            [['User_Id'], 'exist', 'skipOnError' => true, 'targetClass' => Benutzer::className(), 'targetAttribute' => ['User_Id' => 'IdBenutzer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdSchueler' => 'Id Schueler',
            'SKZ' => 'Skz',
            'Vorname' => 'Vorname',
            'Nachname' => 'Nachname',
            'Geburtsdatum' => 'Geburtsdatum',
            'ImstKontingent' => 'Imst Kontingent',
            'User_Id' => 'User  ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKursanmeldungs()
    {
        return $this->hasMany(Kursanmeldung::className(), ['Schueler_Id' => 'IdSchueler']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Benutzer::className(), ['IdBenutzer' => 'User_Id']);
    }

    /**
     * Gets the username of the user with the id of $this->User_Id
     * @return string
     */
    public function getUserName() {
        return Benutzer::findOne($this->User_Id)->username;
    }
}
