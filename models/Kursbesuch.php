<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kursbesuch".
 *
 * @property int $IdKursbesuch
 * @property string $Datum
 * @property int $Schulstunden
 * @property int $Anmeldung_Id
 * @property int $Lehrer_Id
 *
 * @property Kursanmeldung $anmeldung
 * @property Lehrer $lehrer
 */
class Kursbesuch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kursbesuch';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Datum'], 'safe'],
            [['Schulstunden', 'Anmeldung_Id', 'Lehrer_Id'], 'integer'],
            [['Anmeldung_Id', 'Lehrer_Id'], 'required'],
            [['Anmeldung_Id'], 'exist', 'skipOnError' => true, 'targetClass' => Kursanmeldung::className(), 'targetAttribute' => ['Anmeldung_Id' => 'IdKursanmeldung']],
            [['Lehrer_Id'], 'exist', 'skipOnError' => true, 'targetClass' => Lehrer::className(), 'targetAttribute' => ['Lehrer_Id' => 'IdLehrer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdKursbesuch' => 'Id Kursbesuch',
            'Datum' => 'Datum',
            'Schulstunden' => 'Schulstunden',
            'Anmeldung_Id' => 'Anmeldung  ID',
            'Lehrer_Id' => 'Lehrer  ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnmeldung()
    {
        return $this->hasOne(Kursanmeldung::className(), ['IdKursanmeldung' => 'Anmeldung_Id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLehrer()
    {
        return $this->hasOne(Lehrer::className(), ['IdLehrer' => 'Lehrer_Id']);
    }

    /**
     * @return string
     */
    public function getLehrerName() {
        $lehrer = Lehrer::findOne($this->Lehrer_Id);
        $lehrerName = $lehrer->Nachname . ' ' . $lehrer->Vorname;
        return $lehrerName;
    }

    /**
     * @return string
     */
    public function getLehrerKuerzel() {
        return Lehrer::findOne($this->Lehrer_Id)->Lehrerkuerzel;
    }

    /**
     * @return string
     */
    public function getKurs() {
        $anmeldung = Kursanmeldung::findOne($this->Anmeldung_Id);

        return Kurse::findOne($anmeldung->Kurse_Id)->Name;
    }
}
