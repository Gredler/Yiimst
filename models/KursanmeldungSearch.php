<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kursanmeldung;

/**
 * KursanmeldungSearch represents the model behind the search form of `app\models\Kursanmeldung`.
 */
class KursanmeldungSearch extends Kursanmeldung
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdKursanmeldung', 'Schueler_Id', 'Kurse_Id'], 'integer'],
            [['Anmeldedatum', 'Abmeldedatum'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kursanmeldung::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'IdKursanmeldung' => $this->IdKursanmeldung,
            'Schueler_Id' => $this->Schueler_Id,
            'Kurse_Id' => $this->Kurse_Id,
            'Anmeldedatum' => $this->Anmeldedatum,
            'Abmeldedatum' => $this->Abmeldedatum,
        ]);

        return $dataProvider;
    }
}
