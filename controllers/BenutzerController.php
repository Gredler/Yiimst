<?php

namespace app\controllers;

use Yii;
use app\models\Benutzer;
use app\models\BenutzerSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BenutzerController implements the CRUD actions for Benutzer model.
 */
class BenutzerController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Benutzer models.
	 * @return mixed
	 * @throws ForbiddenHttpException
	 */
	public function actionIndex()
	{
		if (Yii::$app->user->can('admin'))
		{
            $searchModel = new BenutzerSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

			return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
			]);
		}
		else
		{
			Throw new ForbiddenHttpException("Sie drüfen das nicht.");
		}
	}

	/**
	 * Displays a single Benutzer model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws ForbiddenHttpException
	 */
	public function actionView($id)
	{
		if (Yii::$app->user->can('admin'))
		{
			return $this->render('view', [
				'model' => $this->findModel($id),
			]);
		}
		else
		{
			Throw new ForbiddenHttpException("Sie drüfen das nicht.");
		}
	}

	/**
	 * Creates a new Benutzer model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws ForbiddenHttpException
	 */
	public function actionCreate()
	{
		if (Yii::$app->user->can('admin'))
		{
			$model = new Benutzer();
			$model->hashPassword = true;

			if ($model->load(Yii::$app->request->post()) && $model->save())
			{
				return $this->redirect(['view', 'id' => $model->IdBenutzer]);
			}

			return $this->render('create', [
				'model' => $model,
			]);
		}
		else
		{
			Throw new ForbiddenHttpException("Sie drüfen das nicht.");
		}
	}

	/**
	 * Updates an existing Benutzer model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws ForbiddenHttpException
	 */
	public function actionUpdate($id)
	{
		if (Yii::$app->user->can('admin'))
		{
			$model = $this->findModel($id);

			if ($model->load(Yii::$app->request->post()) && $model->save())
			{
				return $this->redirect(['view', 'id' => $model->IdBenutzer]);
			}

			return $this->render('update', [
				'model' => $model,
			]);
		}
		else
		{
			Throw new ForbiddenHttpException("Sie drüfen das nicht.");
		}
	}

	/**
	 * Deletes an existing Benutzer model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws ForbiddenHttpException
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete($id)
	{
		if (Yii::$app->user->can('admin'))
		{
			$this->findModel($id)->delete();

			return $this->redirect(['index']);
		}
		else
		{
			Throw new ForbiddenHttpException("Sie drüfen das nicht.");
		}
	}

	/**
	 * Finds the Benutzer model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Benutzer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Benutzer::findOne($id)) !== null)
		{
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
