<?php

namespace app\controllers;

use Yii;
use app\models\Kurse;
use app\models\KurseSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KurseController implements the CRUD actions for Kurse model.
 */
class KurseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kurse models.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('schueler')) {
            $searchModel = new KurseSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            Throw new ForbiddenHttpException("Sie drüfen das nicht.");
        }
    }

    /**
     * Displays a single Kurse model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('lehrer')) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            Throw new ForbiddenHttpException("Sie drüfen das nicht.");
        }
    }

    /**
     * Creates a new Kurse model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('lehrer')) {
            $model = new Kurse();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->IdKurse]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        } else {
            Throw new ForbiddenHttpException("Sie drüfen das nicht.");
        }
    }

    /**
     * Updates an existing Kurse model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can('lehrer')) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->IdKurse]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            Throw new ForbiddenHttpException("Sie drüfen das nicht.");
        }
    }

    /**
     * Deletes an existing Kurse model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('admin')) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            Throw new ForbiddenHttpException("Sie drüfen das nicht.");
        }
    }

    /**
     * Finds the Kurse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Kurse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kurse::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
