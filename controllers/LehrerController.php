<?php

namespace app\controllers;

use Yii;
use app\models\Lehrer;
use app\models\LehrerSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LehrerController implements the CRUD actions for Lehrer model.
 */
class LehrerController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Lehrer models.
	 * @return mixed
	 * @throws ForbiddenHttpException
	 */
	public function actionIndex()
	{
		if (Yii::$app->user->can('admin'))
		{
			$searchModel  = new LehrerSearch();
			$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

			return $this->render('index', [
				'searchModel'  => $searchModel,
				'dataProvider' => $dataProvider,
			]);
		}
		else
		{
			Throw new ForbiddenHttpException("Sie drüfen das nicht.");
		}
	}

	/**
	 * Displays a single Lehrer model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws ForbiddenHttpException
	 */
	public function actionView($id)
	{
		if (Yii::$app->user->can('admin'))
		{
			return $this->render('view', [
				'model' => $this->findModel($id),
			]);
		}
		else
		{
			Throw new ForbiddenHttpException("Sie drüfen das nicht.");
		}
	}

	/**
	 * Creates a new Lehrer model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws ForbiddenHttpException
	 */
	public function actionCreate()
	{
		if (Yii::$app->user->can('admin'))
		{
			$model = new Lehrer();

			if ($model->load(Yii::$app->request->post()) && $model->save())
			{
				return $this->redirect(['view', 'id' => $model->IdLehrer]);
			}

			return $this->render('create', [
				'model' => $model,
			]);
		}
		else
		{
			Throw new ForbiddenHttpException("Sie drüfen das nicht.");
		}
	}

	/**
	 * Updates an existing Lehrer model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws ForbiddenHttpException
	 */
	public function actionUpdate($id)
	{
		if (Yii::$app->user->can('admin'))
		{
			$model = $this->findModel($id);

			if ($model->load(Yii::$app->request->post()) && $model->save())
			{
				return $this->redirect(['view', 'id' => $model->IdLehrer]);
			}

			return $this->render('update', [
				'model' => $model,
			]);
		}
		else
		{
			Throw new ForbiddenHttpException("Sie drüfen das nicht.");
		}
	}

	/**
	 * Deletes an existing Lehrer model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws ForbiddenHttpException
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete($id)
	{
		if (Yii::$app->user->can('admin'))
		{
			$this->findModel($id)->delete();

			return $this->redirect(['index']);
		}
		else
		{
			Throw new ForbiddenHttpException("Sie drüfen das nicht.");
		}
	}

	/**
	 * Finds the Lehrer model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Lehrer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Lehrer::findOne($id)) !== null)
		{
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
