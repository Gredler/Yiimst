<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 17.06.2018
 * Time: 15:49
 */

namespace app\commands;

use app\models\Kursanmeldung;
use app\models\Kurse;
use app\models\Schueler;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class KursanmeldungController extends Controller
{
    public function actionLoadAnmeldungen()
    {
        $anmeldungdaten = [
            [
                'Schueler_Id' => Schueler::findOne(['SKZ' => 'angredler'])->IdSchueler,
                'Kurse_Id' => Kurse::findOne(['Name' => 'POS LAND Schwerpunkt'])->IdKurse,
                'Anmeldedatum' => '2018-06-17',
            ],
            [
                'Schueler_Id' => Schueler::findOne(['SKZ' => 'amurk'])->IdSchueler,
                'Kurse_Id' => Kurse::findOne(['Name' => 'POS LAND Schwerpunkt'])->IdKurse,
                'Anmeldedatum' => '2017-09-17',
            ],
            [
                'Schueler_Id' => Schueler::findOne(['SKZ' => 'amurk'])->IdSchueler,
                'Kurse_Id' => Kurse::findOne(['Name' => 'POS NEUN PHP'])->IdKurse,
                'Anmeldedatum' => '2017-12-17',
            ],
        ];

        foreach ($anmeldungdaten as $daten) {
            $kursanmeldung = new Kursanmeldung($daten);
            if (!$kursanmeldung->save()) {
                print_r($kursanmeldung->getErrors());
            }
        }

        return ExitCode::OK;
    }

    public function actionDeleteAll()
    {
        Kursanmeldung::deleteAll();
    }

    public function actionBootstrap()
    {
        $this->actionDeleteAll();
        $this->actionLoadAnmeldungen();

        return ExitCode::OK;
    }
}