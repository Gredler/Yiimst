<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 14.06.2018
 * Time: 14:09
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class BootstrapController extends Controller
{
	public function actionGo()
	{
		$this->deleteAllData();

		Yii::$app->runAction('benutzer/bootstrap');
		Yii::$app->runAction('lehrer/bootstrap');
		Yii::$app->runAction('schueler/bootstrap');
		Yii::$app->runAction('kurse/bootstrap');
		Yii::$app->runAction('kursanmeldung/bootstrap');
		Yii::$app->runAction('kursbesuch/bootstrap');
	}

	public function deleteAllData()
	{
		Yii::$app->runAction('kursbesuch/delete-all');
		Yii::$app->runAction('kursanmeldung/delete-all');
		Yii::$app->runAction('kurse/delete-all');
		Yii::$app->runAction('lehrer/delete-all');
		Yii::$app->runAction('schueler/delete-all');
		Yii::$app->runAction('benutzer/delete-all');
	}
}