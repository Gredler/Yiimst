<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 14.06.2018
 * Time: 14:57
 */

namespace app\commands;

use app\models\Benutzer;
use app\models\Schueler;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;


class SchuelerController extends Controller
{
	public function actionLoadLehrer()
	{
		$schuelerdaten = [
			[
				'SKZ'            => 'angredler',
				'Vorname'        => 'Andreas',
				'Nachname'       => 'Gredler',
				'Geburtsdatum'   => '1994-02-21',
				'ImstKontingent' => '12',
				'User_Id'        => Benutzer::findOne(['username' => 'schueler'])->IdBenutzer,
			],
			[
				'SKZ'            => 'amurk',
				'Vorname'        => 'Andreas',
				'Nachname'       => 'Murk',
				'Geburtsdatum'   => '1996-07-29',
				'ImstKontingent' => '17',
				'User_Id'        => Benutzer::findOne(['username' => 'murki'])->IdBenutzer,
			],
		];

		foreach ($schuelerdaten as $daten)
		{
			$schueler = new Schueler($daten);
			if (!$schueler->save())
			{
				print_r($schueler->getErrors());
			}
		}

		return ExitCode::OK;
	}

	public function actionDeleteAll()
	{
		Schueler::deleteAll();
	}

	public function actionBootstrap()
	{
		$this->actionDeleteAll();
		$this->actionLoadLehrer();

		return ExitCode::OK;
	}
}