<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 14.06.2018
 * Time: 14:16
 */

namespace app\commands;

use app\models\Benutzer;
use app\models\Lehrer;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class LehrerController extends Controller
{
	public function actionLoadLehrer()
	{
		$lehrerdaten = [
			[
				'Lehrerkuerzel'  => 'SALE',
				'Vorname'        => 'Alexander',
				'Nachname'       => 'Scharmer',
				'ImstKontingent' => '12',
				'User_Id'        => Benutzer::findOne(['username' => 'admin'])->IdBenutzer,
			],
			[
				'Lehrerkuerzel'  => 'LAND',
				'Vorname'        => 'Claudio',
				'Nachname'       => 'Landerer',
				'ImstKontingent' => '17',
				'User_Id'        => Benutzer::findOne(['username' => 'lehrer'])->IdBenutzer,
			],
			[
				'Lehrerkuerzel'  => 'NEUN',
				'Vorname'        => 'Dominik',
				'Nachname'       => 'Neuner',
				'ImstKontingent' => '17',
				'User_Id'        => Benutzer::findOne(['username' => 'neuner'])->IdBenutzer,
			],
		];

		foreach ($lehrerdaten as $daten)
		{
			$lehrer = new Lehrer($daten);
			if (!$lehrer->save())
			{
				print_r($lehrer->getErrors());
			}
		}

		return ExitCode::OK;
	}

	public function actionDeleteAll()
	{
		Lehrer::deleteAll();
	}

	public function actionBootstrap()
	{
		$this->actionDeleteAll();
		$this->actionLoadLehrer();

		return ExitCode::OK;
	}

}