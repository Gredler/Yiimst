<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 17.06.2018
 * Time: 14:50
 */

namespace app\commands;

use app\models\Kurse;
use app\models\Lehrer;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class KurseController extends Controller
{
    public function actionLoadKurse()
    {
        $kursdaten = [
            [
                'Name' => 'POS LAND Schwerpunkt',
                'MaxTeilnehmer' => 30,
                'Schuljahr' => '2018',
                'Semester' => 'JAHR',
                'Rhytmus' => 'wöchentlich',
                'Wochentag' => 'MI',
                'SchulstundeBeginn' => 13,
                'DauerWochenstunden' => 4,
                'Hauptkategorie' => 'OE',
                'SchulstufeHaupt' => 'alle',
                'SchulformHaupt' => 'alle',
                'Lehrer_IdLehrer' => Lehrer::findOne(['Lehrerkuerzel' => 'LAND'])->IdLehrer,
            ],
            [
                'Name' => 'POS NEUN PHP',
                'MaxTeilnehmer' => 20,
                'Schuljahr' => '2019',
                'Semester' => 'SS',
                'Rhytmus' => 'unregelmaessig',
                'Wochentag' => 'MO',
                'SchulstundeBeginn' => 9,
                'DauerWochenstunden' => 3,
                'Hauptkategorie' => 'FF',
                'SchulstufeHaupt' => '10',
                'SchulformHaupt' => 'HAK',
                'Lehrer_IdLehrer' => Lehrer::findOne(['Lehrerkuerzel' => 'NEUN'])->IdLehrer,
            ],
        ];

        foreach ($kursdaten as $daten)
        {
            $kurse = new Kurse($daten);
            if (!$kurse->save())
            {
                print_r($kurse->getErrors());
            }
        }

        return ExitCode::OK;
    }

    public function actionDeleteAll()
    {
        Kurse::deleteAll();
    }

    public function actionBootstrap()
    {
        $this->actionDeleteAll();
        $this->actionLoadKurse();

        return ExitCode::OK;
    }
}