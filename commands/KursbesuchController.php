<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 17.06.2018
 * Time: 16:13
 */

namespace app\commands;

use app\models\Kursanmeldung;
use app\models\Kursbesuch;
use app\models\Kurse;
use app\models\Lehrer;
use app\models\Schueler;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class KursbesuchController extends Controller
{
    public function actionLoadBesuche()
    {
        $besuchdaten = [
            [
                'Datum' => '2018-06-17',
                'Schulstunden' => 50,
                'Anmeldung_Id' => Kursanmeldung::findOne(
                    [
                        'Schueler_Id' => Schueler::findOne(['SKZ' => 'angredler'])->IdSchueler,
                        'Kurse_Id' => Kurse::findOne(['Name' => 'POS LAND Schwerpunkt'])->IdKurse
                    ])->IdKursanmeldung,
                'Lehrer_Id' => Lehrer::findOne(['Lehrerkuerzel' => 'LAND'])->IdLehrer,
            ],
            [
                'Datum' => '2018-06-18',
                'Schulstunden' => 50,
                'Anmeldung_Id' => Kursanmeldung::findOne(
                    [
                        'Schueler_Id' => Schueler::findOne(['SKZ' => 'amurk'])->IdSchueler,
                        'Kurse_Id' => Kurse::findOne(['Name' => 'POS LAND Schwerpunkt'])->IdKurse
                    ])->IdKursanmeldung,
                'Lehrer_Id' => Lehrer::findOne(['Lehrerkuerzel' => 'LAND'])->IdLehrer,
            ],
        ];

        foreach ($besuchdaten as $daten)
        {
            $kursbesuch = new Kursbesuch($daten);
            if (!$kursbesuch->save())
            {
                print_r($kursbesuch->getErrors());
            }
        }

        return ExitCode::OK;
    }

    public function actionDeleteAll()
    {
        Kursbesuch::deleteAll();
    }

    public function actionBootstrap()
    {
        $this->actionDeleteAll();
        $this->actionLoadBesuche();

        return ExitCode::OK;
    }
}