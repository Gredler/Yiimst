<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Benutzer;
use app\rbac\DeleteRule;
use app\rbac\ProfileRule;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class BenutzerController extends Controller
{

	public function actionLoadBenutzer()
	{
		$benutzerdaten = [
			[
				'username' => 'admin',
				'password' => 'password1!',
				'type'     => 'admin',
			],
			[
				'username' => 'lehrer',
				'password' => 'password1!',
				'type'     => 'lehrer',
			],
			[
				'username' => 'neuner',
				'password' => 'password1!',
				'type'     => 'lehrer',
			],
			[
				'username' => 'schueler',
				'password' => 'password1!',
				'type'     => 'schueler',
			],
			[
				'username' => 'murki',
				'password' => 'password1!',
				'type'     => 'schueler',
			]
		];

		foreach ($benutzerdaten as $daten)
		{
			$benutzer               = new Benutzer($daten);
			$benutzer->hashPassword = true;
			if (!$benutzer->save())
			{
				print_r($benutzer->getErrors());
			}
		}

		return ExitCode::OK;
	}

	public function actionDeleteAll()
	{
		Benutzer::deleteAll();
		$auth = Yii::$app->authManager;
		$auth->removeAllAssignments();
	}

	/**
	 * Berechtigungen für RBAC setzen
	 */
	public function actionPermissions()
	{
		$auth = Yii::$app->authManager;
		$auth->removeAllPermissions();

		$updateBenutzer              = $auth->createPermission('updateBenutzer');
		$updateBenutzer->description = 'Benutzer updaten';
		$auth->add($updateBenutzer);

		$deleteBenutzer              = $auth->createPermission('deleteBenutzer');
		$deleteBenutzer->description = 'Benutzer löschen';
		$auth->add($deleteBenutzer);

		$deleteKurs              = $auth->createPermission('deleteKurs');
		$deleteKurs->description = 'Kurse löschen';
		$auth->add($deleteKurs);

		$createKurs              = $auth->createPermission('createKurs');
		$createKurs->description = 'Kurse erstellen';
		$auth->add($createKurs);

		$updateKurs              = $auth->createPermission('updateKurs');
		$updateKurs->description = 'Kurse bearbeiten';
		$auth->add($updateKurs);

		$updateKursanmeldung = $auth->createPermission('updateKursanmeldung');
		$updateKursanmeldung->description = 'Kusanmeldung ändern';
        $auth->add($updateKursanmeldung);

		$deleteKursanmeldung = $auth->createPermission('deleteKursanmeldung');
		$deleteKursanmeldung->description = 'Kusanmeldung löschen';
        $auth->add($deleteKursanmeldung);

        $deleteKursbesuch = $auth->createPermission('deleteKursbesuch');
        $deleteKursbesuch->description = 'Kusbesuch löschen';
        $auth->add($deleteKursbesuch);

        $updateKursbesuch = $auth->createPermission('updateKursbesuch');
        $updateKursbesuch->description = 'Kursbesuch bearbeiten';
        $auth->add($updateKursbesuch);
	}

	/**
	 * Rollen für RBAC setzen
	 */
	public function actionRoles()
	{
		$auth = Yii::$app->authManager;
		$auth->removeAllRoles();

		$updateBenutzer = $auth->getPermission('updateBenutzer');
		$deleteBenutzer = $auth->getPermission('deleteBenutzer');
		$deleteKurs = $auth->getPermission('deleteKurs');
		$createKurs = $auth->getPermission('createKurs');
		$updateKurs = $auth->getPermission('updateKurs');
		$updateKursanmeldung = $auth->getPermission('updateKursanmeldung');
		$deleteKursanmeldung = $auth->getPermission('deleteKursanmeldung');
        $deleteKursbesuch = $auth->getPermission('deleteKursbesuch');
        $updateKursbesuch = $auth->getPermission('updateKursbesuch');

		$schueler = $auth->createRole('schueler');
		$auth->add($schueler);

		$lehrer = $auth->createRole('lehrer');
		$auth->add($lehrer);
		$auth->addChild($lehrer, $schueler);
		$auth->addChild($lehrer, $updateBenutzer);
		$auth->addChild($lehrer, $deleteBenutzer);
        $auth->addChild($lehrer, $createKurs);
        $auth->addChild($lehrer, $updateKurs);
		$auth->addChild($lehrer, $deleteKursanmeldung);
		$auth->addChild($lehrer, $updateKursbesuch);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
		$auth->addChild($admin, $lehrer); // alle Permissions von Lehrer
        $auth->addChild($admin, $deleteKurs);
        $auth->addChild($admin, $updateKursanmeldung);
        $auth->addChild($admin, $deleteKursbesuch);
    }

	/**
	 * Regeln für RBAC setzen
	 */
	public function actionRules()
	{
		$auth = Yii::$app->authManager;
		$auth->removeAllRules();

		$rule = new ProfileRule();
		$auth->add($rule);
		$updateBenutzer           = $auth->getPermission('updateBenutzer');
		$updateBenutzer->ruleName = $rule->name;
		$auth->update('updateBenutzer', $updateBenutzer);

		$rule2 = new DeleteRule();
		$auth->add($rule2);
		$deleteBenutzer           = $auth->getPermission('deleteBenutzer');
		$deleteBenutzer->ruleName = $rule2->name;
		$auth->update('deleteBenutzer', $deleteBenutzer);
	}

	public function actionBootstrap()
	{
		$this->actionDeleteAll();
		$this->actionPermissions();
		$this->actionRoles();
		$this->actionRules();
		$this->actionLoadBenutzer();

		return ExitCode::OK;
	}

}