<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Yiimst';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>

        <p class="lead">HTL Imst.</p>

        <p><?= Yii::$app->user->isGuest ?
			    (Html::a('Login', ['site/login'], ['class' => 'btn btn-lg btn-success'])):
			    (
				    Html::beginForm(['/site/logout'], 'post')
				    . Html::submitButton(
					    'Logout (' . Yii::$app->user->identity->username . ')', //hier von->username geändert ind ->email
					    ['class' => 'btn btn-link logout']
				    )
				    . Html::endForm()
			    ) ?>
        </p>
    </div>

    <div class="body-content">
    </div>
</div>
