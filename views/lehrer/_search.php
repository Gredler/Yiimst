<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LehrerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lehrer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'IdLehrer') ?>

    <?= $form->field($model, 'Lehrerkuerzel') ?>

    <?= $form->field($model, 'Vorname') ?>

    <?= $form->field($model, 'Nachname') ?>

    <?= $form->field($model, 'ImstKontingent') ?>

    <?php // echo $form->field($model, 'User_Id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
