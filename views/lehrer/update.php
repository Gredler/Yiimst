<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lehrer */

$this->title = 'Update Lehrer: ' . $model->IdLehrer;
$this->params['breadcrumbs'][] = ['label' => 'Lehrers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IdLehrer, 'url' => ['view', 'id' => $model->IdLehrer]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lehrer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
