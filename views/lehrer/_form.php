<?php

use app\models\Benutzer;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Lehrer */
/* @var $form yii\widgets\ActiveForm */

$users = ArrayHelper::map(Benutzer::find()->all(), 'IdBenutzer', 'username');
?>

<div class="lehrer-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'Lehrerkuerzel')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Vorname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Nachname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'ImstKontingent')->textInput() ?>

    <?= $form->field($model, 'User_Id')->dropDownList($users, ['prompt' => 'choose one...']); ?>

    <div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>
