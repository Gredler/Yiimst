<?php

use app\models\Kursanmeldung;
use app\models\Lehrer;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Kursbesuch */
/* @var $form yii\widgets\ActiveForm */

$lehrer = ArrayHelper::map(Lehrer::find()->all(), 'IdLehrer', 'Lehrerkuerzel');
$anmeldungen = ArrayHelper::map(Kursanmeldung::find()->all(), 'IdKursanmeldung', 'kursAndSchueler');
?>

<div class="kursbesuch-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Datum')->widget(DatePicker::className(), [
        'options' => ['placeholder' => 'Select date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'Schulstunden')->textInput() ?>

    <?= $form->field($model, 'Anmeldung_Id')->dropDownList($anmeldungen, ['prompt' => 'choose one...']); ?>

    <?= $form->field($model, 'Lehrer_Id')->dropDownList($lehrer, ['prompt' => 'choose one...']); ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
