<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kursbesuch */

$this->title = $model->IdKursbesuch;
$this->params['breadcrumbs'][] = ['label' => 'Kursbesuches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kursbesuch-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->IdKursbesuch], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->IdKursbesuch], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IdKursbesuch',
            'Datum',
            'Schulstunden',
            'Anmeldung_Id',
            'kurs',
            'lehrerName',
            'lehrerKuerzel',
        ],
    ]) ?>

</div>
