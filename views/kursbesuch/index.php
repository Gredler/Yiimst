<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KursbesuchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kursbesuche';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kursbesuch-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kursbesuch', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Datum',
            'Schulstunden',
            'kurs',
            'lehrerKuerzel',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                        //TODO: only let the Lehrer of the model update, don't restrict it to the admin
                    'update' =>
                        function ($url, $model, $key) {

                            if (Yii::$app->user->can('updateKursbesuch', $url)) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->IdKursbesuch]);
                            }
                        },
                    'delete' =>
                        function ($url, $model, $key) {
                            if (Yii::$app->user->can('deleteKursbesuch', $url)) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->IdKursbesuch], [
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                        'title' => Yii::t('yii', 'Delete'),
                                        'aria-label' => Yii::t('yii', 'Delete'),
                                        'data-pjax' => '0',
                                    ],
                                ]);
                            }
                        },
                ],
            ],
        ],
    ]); ?>
</div>
