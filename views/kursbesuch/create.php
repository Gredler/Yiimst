<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kursbesuch */

$this->title = 'Create Kursbesuch';
$this->params['breadcrumbs'][] = ['label' => 'Kursbesuches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kursbesuch-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
