<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kursbesuch */

$this->title = 'Update Kursbesuch: ' . $model->IdKursbesuch;
$this->params['breadcrumbs'][] = ['label' => 'Kursbesuches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IdKursbesuch, 'url' => ['view', 'id' => $model->IdKursbesuch]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kursbesuch-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
