<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Benutzer */

$this->title = 'Update Benutzer: ' . $model->IdBenutzer;
$this->params['breadcrumbs'][] = ['label' => 'Benutzers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IdBenutzer, 'url' => ['view', 'id' => $model->IdBenutzer]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="benutzer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
