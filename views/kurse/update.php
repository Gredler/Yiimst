<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kurse */

$this->title = 'Update Kurse: ' . $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Kurses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->IdKurse]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kurse-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
