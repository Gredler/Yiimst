<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KurseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kurse';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kurse-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
        if(Yii::$app->user->can('createKurs')) {
            echo Html::a('Create Kurse', ['create'], ['class' => 'btn btn-success']);
        } ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Name',
            'Schuljahr',
            'Semester',
            //'Rhytmus',
            //'Wochentag',
            //'SchulstundeBeginn',
            'DauerWochenstunden',
            //'Hauptkategorie',
            //'Subkategorie',
            //'SchulstufeHaupt',
            //'SchulstufeSub',
            //'SchulformHaupt',
            //'SchulformSub',
            //'Bemerkungen:ntext',
//            'Lehrer_IdLehrer',
            'lehrerKuerzel',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'update' =>
                        function ($url, $model, $key) {
                            if (Yii::$app->user->can('updateKurs', $url)) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->IdKurse]);
                            }
                        },
                    'delete' =>
                        function ($url, $model, $key) {
                            if (Yii::$app->user->can('deleteKurs', $url)) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->IdKurse], [
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                        'title' => Yii::t('yii', 'Delete'),
                                        'aria-label' => Yii::t('yii', 'Delete'),
                                        'data-pjax' => '0',
                                    ],
                                ]);
                            }
                        },
                ],
            ],
        ],
    ]); ?>
</div>
