<?php

use app\models\Lehrer;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KurseSearch */
/* @var $form yii\widgets\ActiveForm */

$lehrer = ArrayHelper::map(Lehrer::find()->all(), 'IdLehrer', 'Lehrerkuerzel');
?>

<div class="kurse-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MaxTeilnehmer') ?>

    <?= $form->field($model, 'Schuljahr')->dropDownList([ 2018 => '2018', 2019 => '2019', 2020 => '2020', 2021 => '2021', 2022 => '2022', 2023 => '2023', 2024 => '2024', 2025 => '2025', 2026 => '2026', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'Semester')->dropDownList([ 'WS' => 'WS', 'SS' => 'SS', 'JAHR' => 'JAHR', ], ['prompt' => '']) ?>

    <?php // echo $form->field($model, 'Rhytmus') ?>

    <?php // echo $form->field($model, 'Wochentag') ?>

    <?php // echo $form->field($model, 'SchulstundeBeginn') ?>

    <?php // echo $form->field($model, 'DauerWochenstunden') ?>

    <?php // echo $form->field($model, 'Hauptkategorie') ?>

    <?php // echo $form->field($model, 'Subkategorie') ?>

    <?php // echo $form->field($model, 'SchulstufeHaupt') ?>

    <?php // echo $form->field($model, 'SchulstufeSub') ?>

    <?php // echo $form->field($model, 'SchulformHaupt') ?>

    <?php // echo $form->field($model, 'SchulformSub') ?>

    <?php // echo $form->field($model, 'Bemerkungen') ?>

    <?php // echo $form->field($model, 'Lehrer_IdLehrer') ?>

    <?php  echo $form->field($model, 'Lehrer_IdLehrer')->dropDownList($lehrer, ['prompt' => 'choose one...']) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
