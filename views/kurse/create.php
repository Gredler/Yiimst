<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kurse */

$this->title = 'Create Kurse';
$this->params['breadcrumbs'][] = ['label' => 'Kurses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kurse-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
