<?php

use app\models\Lehrer;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kurse */
/* @var $form yii\widgets\ActiveForm */

$lehrer = ArrayHelper::map(Lehrer::find()->all(), 'IdLehrer', 'Lehrerkuerzel');
?>

<div class="kurse-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MaxTeilnehmer')->textInput() ?>

    <?= $form->field($model, 'Schuljahr')->dropDownList([ 2018 => '2018', 2019 => '2019', 2020 => '2020', 2021 => '2021', 2022 => '2022', 2023 => '2023', 2024 => '2024', 2025 => '2025', 2026 => '2026', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'Semester')->dropDownList([ 'WS' => 'WS', 'SS' => 'SS', 'JAHR' => 'JAHR', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'Rhytmus')->dropDownList([ 'wöchentlich' => 'Wöchentlich', '14taegig' => '14taegig', 'unregelmaessig' => 'Unregelmaessig', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'Wochentag')->dropDownList([ 'MO' => 'MO', 'DI' => 'DI', 'MI' => 'MI', 'DO' => 'DO', 'FR' => 'FR', 'SA' => 'SA', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'SchulstundeBeginn')->textInput() ?>

    <?= $form->field($model, 'DauerWochenstunden')->textInput() ?>

    <?= $form->field($model, 'Hauptkategorie')->dropDownList([ 'FF' => 'FF', 'ZA' => 'ZA', 'BF' => 'BF', 'OE' => 'OE', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'Subkategorie')->dropDownList([ 'FF' => 'FF', 'ZA' => 'ZA', 'BF' => 'BF', 'OE' => 'OE', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'SchulstufeHaupt')->dropDownList([ 9 => '9', 10 => '10', 11 => '11', 12 => '12', 13 => '13', 'alle' => 'Alle', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'SchulstufeSub')->dropDownList([ 9 => '9', 10 => '10', 11 => '11', 12 => '12', 13 => '13', 'alle' => 'Alle', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'SchulformHaupt')->dropDownList([ 'HAS' => 'HAS', 'HAK' => 'HAK', 'HMW' => 'HMW', 'HWI' => 'HWI', 'HST' => 'HST', 'alle' => 'Alle', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'SchulformSub')->dropDownList([ 'HAS' => 'HAS', 'HAK' => 'HAK', 'HMW' => 'HMW', 'HWI' => 'HWI', 'HST' => 'HST', 'alle' => 'Alle', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'Bemerkungen')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Lehrer_IdLehrer')->dropDownList($lehrer, ['prompt' => 'choose one...']); ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
