<?php

use app\models\Kurse;
use app\models\Schueler;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\KursanmeldungSearch */
/* @var $form yii\widgets\ActiveForm */

$schueler = ArrayHelper::map(Schueler::find()->all(), 'IdSchueler', 'SKZ');
$kurse = ArrayHelper::map(Kurse::find()->all(), 'IdKurse', 'Name');
?>

<div class="kursanmeldung-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php  echo $form->field($model, 'Schueler_Id')->dropDownList($schueler, ['prompt' => 'choose one...']) ?>

    <?php  echo $form->field($model, 'Kurse_Id')->dropDownList($kurse, ['prompt' => 'choose one...']) ?>


    <?= $form->field($model, 'Anmeldedatum')->widget(DatePicker::className(), [
        'options' => ['placeholder' => 'Select register date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'Abmeldedatum')->widget(DatePicker::className(), [
        'options' => ['placeholder' => 'Select deregister date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
