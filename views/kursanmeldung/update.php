<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kursanmeldung */

$this->title = 'Update Kursanmeldung: ' . $model->IdKursanmeldung;
$this->params['breadcrumbs'][] = ['label' => 'Kursanmeldungs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IdKursanmeldung, 'url' => ['view', 'id' => $model->IdKursanmeldung]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kursanmeldung-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
