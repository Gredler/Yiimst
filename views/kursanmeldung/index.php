<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KursanmeldungSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kursanmeldung';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kursanmeldung-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kursanmeldung', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'schuelerSKZ',
            'kursName',
            'Anmeldedatum',
            'Abmeldedatum',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'update' =>
                        function ($url, $model, $key) {

                            if (Yii::$app->user->can('updateKursanmeldung', $url)) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->IdKursanmeldung]);
                            }
                        },
                    'delete' =>
                        function ($url, $model, $key) {

                            if (Yii::$app->user->can('deleteKursanmeldung', $url)) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->IdKursanmeldung], [
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                        'title' => Yii::t('yii', 'Delete'),
                                        'aria-label' => Yii::t('yii', 'Delete'),
                                        'data-pjax' => '0',
                                    ],
                                ]);
                            }
                        },
                ],
            ],
        ],
    ]); ?>
</div>
