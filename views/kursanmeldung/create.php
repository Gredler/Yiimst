<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kursanmeldung */

$this->title = 'Create Kursanmeldung';
$this->params['breadcrumbs'][] = ['label' => 'Kursanmeldungs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kursanmeldung-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
