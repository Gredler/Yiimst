<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Schueler */

$this->title = 'Update Schueler: ' . $model->IdSchueler;
$this->params['breadcrumbs'][] = ['label' => 'Schuelers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IdSchueler, 'url' => ['view', 'id' => $model->IdSchueler]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="schueler-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
