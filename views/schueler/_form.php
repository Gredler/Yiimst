<?php

use app\models\Benutzer;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Schueler */
/* @var $form yii\widgets\ActiveForm */

$users = ArrayHelper::map(Benutzer::find()->all(), 'IdBenutzer', 'username');
?>

<div class="schueler-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'SKZ')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Vorname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nachname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Geburtsdatum')->widget(DatePicker::className(), [
        'options' => ['placeholder' => 'Select birth date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'ImstKontingent')->textInput() ?>

    <?= $form->field($model, 'User_Id')->dropDownList($users, ['prompt' => 'choose one...']); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
