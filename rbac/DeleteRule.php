<?php
/**
 * Created by IntelliJ IDEA.
 * User: Claudio Landerer
 * Date: 08.02.2018
 * Time: 19:50
 */

namespace app\rbac;
use yii\rbac\Item;
use yii\rbac\Rule;
use app\models\Benutzer;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use Yii;

class DeleteRule extends Rule{

    public $name = 'isAdminDelete';

    /**
     * Executes the rule.
     *
     * @param string|int $user the user ID. This should be either an integer or a string representing
     * the unique identifier of a user. See [[\yii\web\User::id]].
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to [[CheckAccessInterface::checkAccess()]].
     * @return bool a value indicating whether the rule permits the auth item it is associated with.
     */
    public function execute($user, $item, $params)
    {
        //in Params steht das drinnen, was als Daten beim user-can-Aufruf mitgegeben wird
        //indiesem Fall ein Objekt vom Typ Benutzer, siehe \Benutzer\index.php -> dort wird $model mitgegeben
        if(isset($params))
        {
            //Löschen geht nur wenn man nicht sich selbst löschen möchte, wenn man admin ist, oder wenn man mitarbeiter ist
            //und nicht den Admin löschen möchte.
            //datensatz kommt als Assoziativ-Array-Eintrag vom Aufrufer der CAN-Methode, siehe z.B. den Aufruf in
            // /Benutzer/index.php -> dort wird das mitgegeben.
            return $user != $params['datensatz']->getId() &&
                (Yii::$app->user->can('admin'));
        } else {
            Yii::getLogger()->log('noparams',1);
            return false;
        }
    }
}









