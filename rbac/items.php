<?php
return [
    'updateBenutzer' => [
        'type' => 2,
        'description' => 'Benutzer updaten',
        'ruleName' => 'ownsProfile',
    ],
    'deleteBenutzer' => [
        'type' => 2,
        'description' => 'Benutzer löschen',
        'ruleName' => 'isAdminDelete',
    ],
    'deleteKurs' => [
        'type' => 2,
        'description' => 'Kurse löschen',
    ],
    'createKurs' => [
        'type' => 2,
        'description' => 'Kurse erstellen',
    ],
    'updateKurs' => [
        'type' => 2,
        'description' => 'Kurse bearbeiten',
    ],
    'updateKursanmeldung' => [
        'type' => 2,
        'description' => 'Kusanmeldung ändern',
    ],
    'deleteKursanmeldung' => [
        'type' => 2,
        'description' => 'Kusanmeldung löschen',
    ],
    'deleteKursbesuch' => [
        'type' => 2,
        'description' => 'Kusbesuch löschen',
    ],
    'updateKursbesuch' => [
        'type' => 2,
        'description' => 'Kursbesuch bearbeiten',
    ],
    'schueler' => [
        'type' => 1,
    ],
    'lehrer' => [
        'type' => 1,
        'children' => [
            'schueler',
            'updateBenutzer',
            'deleteBenutzer',
            'createKurs',
            'updateKurs',
            'deleteKursanmeldung',
            'updateKursbesuch',
        ],
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'lehrer',
            'deleteKurs',
            'updateKursanmeldung',
            'deleteKursbesuch',
        ],
    ],
];
