<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 07.05.2018
 * Time: 10:59
 */

namespace app\rbac;

use yii\rbac\Item;
use yii\rbac\Rule;


class ProfileRule extends Rule
{

	public $name = 'ownsProfile';

	/**
	 * Executes the rule.
	 *
	 * @param string|int $user   the user ID. This should be either an integer or a string representing
	 *                           the unique identifier of a user. See [[\yii\web\User::id]].
	 * @param Item       $item   the role or permission that this rule is associated with
	 * @param array      $params parameters passed to [[CheckAccessInterface::checkAccess()]].
	 *benutzer
	 * @return bool a value indicating whether the rule permits the auth item it is associated with.
	 */
	public function execute($user, $item, $params)
	{
		return (isset($params)) ? ($user === $params['user']->getId()) : false;
	}
}